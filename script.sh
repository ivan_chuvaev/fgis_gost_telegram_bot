IMAGE_NAME='fgis_bot'
VOLUME_HOST_PATH="$PWD/fgis_volume"
VOLUME_MOUNT_POINT="/usr/src/app/fgis_volume"

function stop_container_if_exist(){
    containers_ids=$(sudo docker ps -f ancestor=$IMAGE_NAME --format '{{.ID}}')
    if [[ $containers_ids ]]; then

        echo "stopping and removing ${IMAGE_NAME} container"
        sudo docker rm $(sudo docker stop $containers_ids)
    
        return 0

    else

        return 1

    fi
}

if [[ "$1" == "build" ]]; then

    stop_container_if_exist

    previous_image_id=$(sudo docker images --format '{{.Repository}}: {{.ID}}' | grep $IMAGE_NAME | sed 's/^.*: //')

    echo "building ${IMAGE_NAME} image"
    sudo docker build --tag $IMAGE_NAME .

    if [[ $previous_image_id ]]; then

        echo "removing previous image"
        sudo docker rmi $previous_image_id
        
    fi

    echo "starting container for $IMAGE_NAME image"
    sudo docker run -d -v $VOLUME_HOST_PATH:$VOLUME_MOUNT_POINT $IMAGE_NAME

elif [[ "$1" == "start" ]]; then

    #if image exist
    if [[ $(sudo docker images --format '{{.Repository}}' | grep $IMAGE_NAME) ]]; then

        stop_container_if_exist

        echo "starting container for $IMAGE_NAME image"
        sudo docker run -d -v $VOLUME_HOST_PATH:$VOLUME_MOUNT_POINT $IMAGE_NAME


    else

        echo "image $IMAGE_NAME does not exists"

    fi

elif [[ "$1" == "stop" ]]; then

    if ! stop_container_if_exist ; then
        echo "container ${IMAGE_NAME} does not exists"
    fi

else

    if [[ "$1" ]]; then
        echo "incorrect input argument: $1"
    else
        echo "no input arguments. Should be one of: [build, start, stop]"
    fi

fi