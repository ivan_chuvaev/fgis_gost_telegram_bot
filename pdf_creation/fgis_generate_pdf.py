import os
import qrcode
import urllib.request
import json
import logging
from pdf_creation.pub_fsa_gov_request import get_reg_numbers_by_shifer
from pdf_creation.fgis_fpdf_class import PDF, Cell_style
from src.volume_files import get_text_line
from src.config import *

months=['января',"февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"]

def get_nested_value(json_obj, arr):
    for i in range(len(arr)):
        if arr[i] in json_obj:
            json_obj = json_obj[arr[i]]
        else:
            return '-'
    return json_obj

def fgis_generate_pdf(vri_id, name):

    two_cells_style = [
        Cell_style(align='LM', border=''),
        Cell_style(cell_size = 2/3, align='LM', border='B', font_family=Cell_style.sbff)
    ]

    three_cells_style =[
        Cell_style(align='LM', border=''),
        Cell_style(align='LM', border='B', font_family=Cell_style.sbff),
        Cell_style(align='LM', border='B', font_family=Cell_style.sbff)
    ]
        
    url = get_text_line('device_info_json') % (vri_id)
    logging.info('retrieving data about device: {}'.format(url))
    with urllib.request.urlopen( urllib.request.Request(url, headers=fgis_device_info_headers), timeout=fgis_request_timeout) as response:
        res_str = response.read().decode('utf-8')
        json_obj = json.loads(res_str)

        if not 'result' in json_obj or not 'vriInfo' in json_obj['result']:
            return(-1)

        pdf = PDF(orientation='P', unit='mm', format='A5')
        pdf.add_page()
        pdf.set_font(Cell_style.sff, '', Cell_style.sfs)
        pdf.table_one_line([json_obj['result']['vriInfo']['organization']])
        pdf.table_one_line(['Регистрационный номер в реестре аккредитованных лиц'])
        pdf.set_font(Cell_style.sbff, '', 9)

        reg_numbers = get_reg_numbers_by_shifer(json_obj['result']['vriInfo']['signCipher'])
        pdf.table_one_line(['; '.join(reg_numbers)])
        pdf.set_font(Cell_style.sbff, '', Cell_style.sbfs)
        pdf.table_one_line(['Выписка из Федерального информационного фонда'])

        bm_save = pdf.b_margin
        pdf.set_auto_page_break(False)
        pdf.b_margin = bm_save

        pdf.set_font(Cell_style.sff, '', Cell_style.sfs)

        applicable = False
        if 'applicable' in json_obj['result']['vriInfo']:
            applicable = True

        if applicable:
            pdf.table_one_line(['№ ' + get_nested_value(json_obj, ['result','vriInfo','applicable','certNum'])])
        else:
            pdf.table_one_line(['№ ' + get_nested_value(json_obj, ['result','vriInfo','inapplicable','noticeNum'])])

        pdf.set_y(pdf.get_y() + 0.01 * pdf.h)

        validDate = get_nested_value(json_obj, ['result','vriInfo','validDate'])
        if validDate != '-':
            validDate = json_obj['result']['vriInfo']['validDate'].split('.')
            pdf.table_one_line(['СИ принадлежит:', get_nested_value(json_obj, ['result','vriInfo','miOwner']), 'Действительно\nдо «'+
            validDate[0]+'» '+ months[int(validDate[1])-1] +' '+validDate[2]+' года'], cell_styles=three_cells_style)
        else:
            pdf.table_one_line(['СИ принадлежит:', get_nested_value(json_obj, ['result','vriInfo','miOwner']), '-' ], cell_styles=three_cells_style)

        if applicable:
            pdf.table_one_line(['Результат поверки:', 'годен'], cell_styles=two_cells_style)
        else:
            pdf.table_one_line(['Результат поверки:', 'не годен'], cell_styles=two_cells_style)
        

        #------------------------------------------------------------------------------------------------------------------------------------------------------------
        #------------------------------------------------ miInfo and singleMI ---------------------------------------------------------------------------------------
        #------------------------------------------------------------------------------------------------------------------------------------------------------------
        
        
        if 'miInfo' in json_obj['result'] and 'singleMI' in json_obj['result']['miInfo']:
            pdf.table_one_line(['Средство измерений:',
                get_nested_value(json_obj, ['result','miInfo','singleMI','mitypeTitle']) + ' ' +
                get_nested_value(json_obj, ['result','miInfo','singleMI','mitypeType']) + ' ' +
                'мод. ' + get_nested_value(json_obj, ['result','miInfo','singleMI','modification']),
                'рег. № '+ get_nested_value(json_obj, ['result','miInfo','singleMI','mitypeNumber'])], cell_styles=three_cells_style)
            pdf.table_one_line(['заводской (серийный) номер:', get_nested_value(json_obj, ['result','miInfo','singleMI','manufactureNum'])], cell_styles=two_cells_style)


        #------------------------------------------------------------------------------------------------------------------------------------------------------------
        #------------------------------------------------------------------------------------------------------------------------------------------------------------


        pdf.table_one_line(['в составе:', '-'], cell_styles=two_cells_style)

        two_cells_style[1].sub = 'наименование единиц величин, диапазонов измерений, на которых поверено средство измерений'
        pdf.table_one_line(['поверено:', '-'], cell_styles=two_cells_style)

        two_cells_style[1].sub = 'наименование или обозначение документа, на основании которого выполнена поверка'
        pdf.table_one_line(['в соответствии с', get_nested_value(json_obj, ['result','vriInfo','docTitle'])], cell_styles=two_cells_style)

        two_cells_style[1].sub = 'регистрационный номер и (или) наименование, тип'
        pdf.table_one_line(['с применением эталонов:', 'см. на обороте'], cell_styles=two_cells_style, border='')

        pdf.table_one_line([''], cell_styles=[
            Cell_style(align='C',
            sub = 'заводской номер, разряд, класс или погрешность эталонов, применяемых при поверке')
            ],  border='B')

        two_cells_style[1].sub = 'перечень влияющих факторов'
        pdf.table_one_line(['при следующих значениях влияющих факторов:', '-'], cell_styles=two_cells_style, border='')

        pdf.table_one_line(['-'], cell_styles=[
            Cell_style(align='C', font_family=Cell_style.sbff,
            sub = 'нормированных в документе на методику поверки, с указанием их значений')
            ],  border='B')

        tmp =['','']
        if get_nested_value(json_obj, ['result','vriInfo','vriType']) == 1:
            tmp[0] = 'первичной'
        else:
            tmp[0] = 'периодической'
        if applicable:
            tmp[1] = 'пригодным'
        else:
            tmp[1] = 'непригодным'
        
        pdf.table_one_line(['и на основании результатов '+tmp[0]+' поверки признано '+tmp[1]+' к применению в объеме проведенной поверки.'], cell_styles=[
            Cell_style(align='L')
            ])

        pdf.table_one_line(['условный шифр знака поверки: ' + get_nested_value(json_obj, ['result','vriInfo','signCipher'])], cell_styles=[Cell_style(align='L')])
        pdf.ln()
        pdf.table_one_line(['Номер записи о результатах поверки в ФИФ:', url.split('/')[-1]], cell_styles=[
            Cell_style(cell_size=0.5, align='LM', font_family=Cell_style.sbff),
            ], border='')
        pdf.table_one_line(['Дата поверки:'], cell_styles=[Cell_style(align='L')])

        vrfDate = get_nested_value(json_obj, ['result','vriInfo','vrfDate'])
        if vrfDate != '-':
            vrfDate = vrfDate.split('.')
            pdf.table_one_line(['«'+vrfDate[0]+'» ' + months[int(vrfDate[1])-1] + ' ' + vrfDate[2] + ' года'], cell_styles=[Cell_style(align='L', font_family=Cell_style.sbff)])
        else:
            pdf.table_one_line(['-'], cell_styles=[Cell_style(align='L', font_family=Cell_style.sbff)])

        pdf.ln()


        #---------------------------------- QR code ----------------------------------------------


        qr = qrcode.QRCode(
            version=3,
            error_correction=qrcode.constants.ERROR_CORRECT_M,
            box_size=10,
            border=0,
        )

        qr.add_data('https://fgis.gost.ru/fundmetrology/cm/results/' + vri_id)
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")

        img_name = name[:name.rfind('.')] + '.png'
        img.save(img_name)

        img_size = 20 #mm

        if pdf.get_y() + img_size > pdf.h - pdf.b_margin:
            pdf.add_page()
            pdf.set_xy(pdf.l_margin, pdf.t_margin)
        
        #draw qr code image
        if pdf.page == 1:
            pdf.set_y(pdf.h - pdf.b_margin - img_size)

        save_y = pdf.get_y()
        pdf.set_x(pdf.w - pdf.r_margin - img_size)
        pdf.image(img_name, w=img_size)

        os.remove(img_name)


        #--------------------------------------------- Link to web page ----------------------------------------------------------


        #check height of link's cell
        pdf.set_xy(pdf.w, 0)
        pdf.table_one_line(['https://fgis.gost.ru/fundmetrology/cm/results/' + vri_id], cell_styles=[Cell_style(align='L', cell_size=(pdf.w - img_size - pdf.r_margin)/pdf.w)], border='')
        lh = pdf.get_y()

        #draw link
        pdf.set_xy(pdf.l_margin, save_y + img_size - lh)
        pdf.table_one_line(['https://fgis.gost.ru/fundmetrology/cm/results/' + vri_id], cell_styles=[Cell_style(align='L', cell_size=(pdf.w - img_size - pdf.r_margin)/pdf.w)], border='')
        pdf.set_x(pdf.l_margin)

        if pdf.page == 1:
            pdf.add_page()
            pdf.set_xy(pdf.l_margin, pdf.t_margin)
        else:
            pdf.ln()


        #-------------------------------------------- everything that placed in means --------------------------------------------------


        if 'means' in json_obj['result']:

            if 'uve' in json_obj['result']['means']:

                pdf.set_font(Cell_style.sbff, '', Cell_style.sbfs)
                pdf.table_one_line(['Эталоны'])
                pdf.ln()
                pdf.set_font(Cell_style.sff, '', Cell_style.sfs)

                tmp = json_obj['result']['means']['uve']
                ethalons = ''
                for el in tmp:
                    ethalons += el['number'] + ' ' + el['title'] + '\n\n'

                pdf.table_one_line([ethalons],
                cell_styles=[Cell_style(align='J')])

            if 'npe' in json_obj['result']['means']:
                pdf.set_font(Cell_style.sbff, '', Cell_style.sbfs)
                pdf.table_one_line(['Государственные первичные эталоны'])
                pdf.ln()
                pdf.set_font(Cell_style.sff, '', Cell_style.sfs)

                tmp = json_obj['result']['means']['npe']
                ethalons = ''
                for el in tmp:
                    ethalons += el['number'] + '; ' + el['title'] + '; ' + '\n\n'

                pdf.table_one_line([ethalons],
                cell_styles=[Cell_style(align='J')])


            if 'mieta' in json_obj['result']['means']:
                pdf.set_font(Cell_style.sbff, '', Cell_style.sbfs)
                pdf.table_one_line(['Средства измерений, применяемые в качестве эталона'])
                pdf.ln()
                pdf.set_font(Cell_style.sff, '', Cell_style.sfs)

                tmp = json_obj['result']['means']['mieta']
                ethalons = ''
                for el in tmp:
                    ethalons += el['regNumber'] + '; ' + el['mitypeNumber'] + '; ' + el['mitypeTitle'] + '; ' +\
                    el['notation'] + '; ' + el['modification'] + '; ' + el['manufactureNum'] + '; ' + str(el['manufactureYear']) +\
                    '; ' + el['rankCode'] + '; ' + el['rankTitle'] + '; ' + el['schemaTitle'] +'\n\n'

                pdf.table_one_line([ethalons],
                cell_styles=[Cell_style(align='J')])
            
            if 'mis' in json_obj['result']['means']:
                pdf.set_font(Cell_style.sbff, '', Cell_style.sbfs)
                pdf.table_one_line(['Средства измерений, применяемые при поверке'])
                pdf.ln()
                pdf.set_font(Cell_style.sff, '', Cell_style.sfs)

                tmp = json_obj['result']['means']['mis']
                ethalons = ''
                for el in tmp:
                    ethalons += el['mitypeNumber'] + '; ' + el['mitypeTitle'] + '; ' + el['number'] + '\n\n'

                pdf.table_one_line([ethalons],
                cell_styles=[Cell_style(align='J')])

            if 'ses' in json_obj['result']['means']:
                pdf.set_font(Cell_style.sbff, '', Cell_style.sbfs)
                pdf.table_one_line(['Стандартные образцы'])
                pdf.ln()
                pdf.set_font(Cell_style.sff, '', Cell_style.sfs)

                tmp = json_obj['result']['means']['ses']
                ethalons = ''
                for el in tmp:
                    ethalons += el['number'] + '; ' + el['title'] + '; ' + str(el['manufactureYear']) + '\n\n'

                pdf.table_one_line([ethalons],
                cell_styles=[Cell_style(align='J')])


        #------------------------------------------------------- Unknown ------------------------------------------------------

        met_characteristics = None

        if met_characteristics != None:
            pdf.ln()
            pdf.set_font(Cell_style.sbff, '', Cell_style.sbfs)
            pdf.table_one_line(['МЕТРОЛОГИЧЕСКИЕ ХАРАКТЕРИСТИКИ И (ИЛИ) ПРОТОКОЛ ПОВЕРКИ'])
            pdf.ln()
            pdf.set_font(Cell_style.sff, '', Cell_style.sfs)
            pdf.table_one_line(['-'],
            cell_styles=[Cell_style(align='J')])

    pdf.output(name,'F')