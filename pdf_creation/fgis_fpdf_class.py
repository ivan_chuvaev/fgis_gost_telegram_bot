from fpdf import FPDF  # fpdf class

class Cell_style:
    sff = 'Arial'
    sfs = 8
    sub_fs = 5
    sbff = 'Arial-B'
    sbfs = 12
    def __init__(self, font_family =sff, font_size=sfs, border='', cell_size=None, align='CM', sub=None):
        self.font_family = font_family
        self.font_size = font_size
        self.border = border
        self.cell_size = cell_size
        self.sub = sub
        if align.find('M') != -1:
            self.center_vertical = True
            align = align.replace('M','')
        else:
            self.center_vertical = False
        self.align = align

class PDF(FPDF):
    arial_dir = 'pdf_creation/Arial/'
    def __init__(self, orientation, unit, format):
        FPDF.__init__(self, orientation, unit, format)
        self.r_margin = 5
        self.l_margin = 5
        self.t_margin = 5
        self.b_margin = 5
        self.line_gap = 0.8
        self.sub_h = 3.0
        self.add_font('Arial', '', self.arial_dir + 'ArialMT.ttf', uni=True)
        self.add_font('Arial-B', '', self.arial_dir + 'Arial-BoldMT.ttf', uni=True)
        self.add_font('Arial-I', '', self.arial_dir + 'Arial-ItalicMT.ttf', uni=True)

    def draw_page_borders(self):
        self.set_line_width(0.0)
        self.line(self.l_margin,self.t_margin,self.r_margin,self.t_margin) # top one
        self.line(self.l_margin,self.b_margin,self.r_margin,self.b_margin) # bottom one
        self.line(self.l_margin,self.t_margin,self.l_margin,self.b_margin) # left one
        self.line(self.r_margin,self.t_margin,self.r_margin,self.b_margin) # right one

    def texts(self,w, txt):
        self.multi_cell(w, self.font_size + self.line_gap, txt, border='', align='C')

    def table_one_line(self, cols, border='', cell_styles=[]):

        save_font_size = self.font_size_pt
        save_font_family = self.font_family

        num_cols = len(cols)
        if num_cols <= 0:
            raise Exception('cols length is 0')

        if len(cell_styles) != num_cols:
            tmp = [None] * num_cols
            for i in range(len(cell_styles)):
                tmp[i] = cell_styles[i]
            for i in range(len(cell_styles), num_cols):
                tmp[i] = Cell_style(font_family=self.font_family, font_size=self.font_size_pt)
            cell_styles = tmp

        ch = self.font_size + self.line_gap
        cur_pos = [self.get_x(), self.get_y()]
        max_y = 0
        last_x = cur_pos[0]

        #calculate width of cells with not specified width
        tmp = 0.0
        none_count = 0
        for i in range(num_cols):
            if cell_styles[i].cell_size != None:
                tmp += cell_styles[i].cell_size
            else:
                none_count+=1
        if tmp > 1.0:
            raise Exception('sum of col_sizes\'s elements is greater than 1.0, it should be equal')
        if none_count != 0:
            cwc = ((1.0-tmp) * (self.w - self.l_margin - self.r_margin)) / none_count
        else:
            cwc = (self.w - self.l_margin - self.r_margin) / num_cols


        #fill array of cells' width
        cws = [None] * num_cols
        for i in range(num_cols):
            if cell_styles[i].cell_size != None:
                cws[i] = cell_styles[i].cell_size * (self.w - self.l_margin - self.r_margin)
            else:
                cws[i] = cwc

        #calculate height of each cell by drawing outside visible part of document
        #find max height
        chs = [None] * num_cols
        max_h = 0
        for i in range(num_cols):
            self.set_xy(self.w,0)
            self.set_font(family = cell_styles[i].font_family, style = '', size = cell_styles[i].font_size)
            ch = self.font_size + self.line_gap
            self.multi_cell(cws[i], ch, cols[i])
                
            chs[i] = self.get_y()
            if chs[i] > max_h:
                max_h = chs[i]
        max_y = cur_pos[1] + max_h

        if max_y > self.h - self.b_margin:
            self.add_page()
            self.set_xy(cur_pos[0], self.t_margin)
            cur_pos[1] = self.t_margin
            max_y = max_h + self.t_margin

        #drawing text
        for i in range(num_cols):
            if cell_styles[i].center_vertical:
                self.set_xy(last_x, cur_pos[1] + float(max_h - chs[i]) / 2.0 )
            else:
                self.set_xy(last_x, cur_pos[1])
            self.set_font(family = cell_styles[i].font_family, style = '', size = cell_styles[i].font_size)
            ch = self.font_size + self.line_gap
            self.multi_cell(cws[i], ch, cols[i], border='', align=cell_styles[i].align)
            if cell_styles[i].sub != None:
                self.set_font(family = Cell_style.sff, style = '', size = Cell_style.sub_fs)
                self.set_xy(last_x, max_y)
                self.multi_cell(cws[i], self.font_size + self.line_gap, cell_styles[i].sub, align='C')
            #draw cell borders
            if border.find('B') == -1 and cell_styles[i].border.find('B') != -1:
                self.line(last_x, max_y, last_x + cws[i], max_y)
            if border.find('T') == -1 and cell_styles[i].border.find('T') != -1:
                self.line(last_x, cur_pos[1], last_x + cws[i], cur_pos[1])
            if border.find('I') == -1 and cell_styles[i].border.find('L') != -1:
                self.line(last_x, cur_pos[1], last_x, max_y)
            if border.find('I') == -1 and cell_styles[i].border.find('R') != -1:
                self.line(last_x + cws[i], cur_pos[1], last_x + cws[i], max_y)
            last_x += cws[i]

        #drawing borders of table
        if border.find('B') != -1:
            self.line(cur_pos[0], max_y, last_x, max_y)
        if border.find('T') != -1:
            self.line(cur_pos[0], cur_pos[1], last_x, cur_pos[1])
        if border.find('L') != -1:
            self.line(cur_pos[0], cur_pos[1], cur_pos[0], max_y)
        if border.find('R') != -1:
            self.line(last_x, cur_pos[1], last_x, max_y)

        #drawing border between cells
        if border.find('I') != -1:
            last_x = cur_pos[0]
            for i in range(num_cols-1):
                last_x += cws[i]
                self.line(last_x, cur_pos[1], last_x, max_y)

        for i in range(num_cols):
            if cell_styles[i].sub != None:
                max_y += self.sub_h  #bad idea
                break
        self.set_xy(cur_pos[0], max_y)
        self.set_font_size(save_font_size)
        self.set_font(save_font_family, '', save_font_size)