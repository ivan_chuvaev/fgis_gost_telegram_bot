import urllib.request, urllib.parse
import json
import ssl
import logging
from src.config import *
import traceback
from socket import timeout

PUB_TOKEN = None

def get_inn_by_shifer(shifer):
	try:
		url = 'https://fgis.gost.ru/fundmetrology/cm/xcdb/vcs/select?fq='+urllib.parse.quote(shifer)+'&q=*&fl=inn&rows=20&start=0'
		
		logging.info('retrieving INN of organization by cipher: {}'.format(url))

		response = urllib.request.urlopen(url, timeout=fgis_request_timeout)
		if response.status != 200:
			raise

		response = json.load(response)
		return ([el['inn'] for el in response['response']['docs']])
	except Exception as e:
		logging.error("cannot get inn by shifer {}, {}".format(shifer, e))
		return '-'
		
def get_token(ssl_context):
	req = urllib.request.Request(
		'https://pub.fsa.gov.ru/login',
		headers={
			'Authorization': 'Bearer null',
			'Host': 'pub.fsa.gov.ru',
			'Content-Type': 'application/json'
		},
		data=json.dumps({
			"username":"anonymous",
			"password":"hrgesf7HDR67Bd"
		}).encode('utf-8')
	)
	try:
		logging.info('retrieving token: {}'.format(req.full_url))
		response = urllib.request.urlopen(req, context=ssl_context, timeout=pub_fsa_gov_ru_request_timeout)
		if response.status != 200:
			raise
	except Exception:
		return -1
	return response.info().get_all('Authorization')[0]
	

def get_reg_numbers(inn, drop=False):
	global PUB_TOKEN
	try:
		ssl_context = ssl.SSLContext()

		if PUB_TOKEN == None:
			PUB_TOKEN = get_token(ssl_context)
			if PUB_TOKEN == -1:
				raise Exception('Cannot get token from pub.fsa.gov.ru')

		req = urllib.request.Request(
			'https://pub.fsa.gov.ru/api/v1/ral/common/showcases/get',
			headers={
				'Authorization':PUB_TOKEN,
				'Host':'pub.fsa.gov.ru',
				'Content-Type':'application/json'
			},
			data=json.dumps({
				"columns":[
					{
						"name":"applicantInn",
						"search":str(inn),
						"type":0
					}
				],
				"sort":["-id"],
				"limit":10,
				"offset":0
			}).encode('utf-8')
		)

		logging.info('retrieving organization data: {}'.format(req.full_url))

		response = urllib.request.urlopen(req, context=ssl_context, timeout=pub_fsa_gov_ru_request_timeout)

		if response.status != 200:
			PUB_TOKEN = None
			if drop:
				raise Exception('pub.fsa.gov.ru refused to give token')
			return get_reg_numbers(inn, True)
		
		return [el['regNumber'] for el in json.load(response)['items']]
	except timeout:
		logging.error('cannot get organization data from pub.fsa.gov.ru, timeout, access token: {}'.format(PUB_TOKEN))
		return '-'
	except Exception as e:
		logging.error(e)
		return '-'


def get_reg_numbers_by_shifer(shifer):
	try:
		inn = get_inn_by_shifer(shifer)[0]
		reg_numbers = get_reg_numbers(inn)
		return reg_numbers
	except:
		return ['-']

