import json
import os
import logging

TEXT_LINES_FILENAME = './fgis_volume/text_lines.json'
MESSAGE_ADVERTISEMENT_FILENAME = './fgis_volume/message_advertisement.txt'

text_lines = None
text_lines_file_updated_time = None

def get_message_advertisement():
    with open(MESSAGE_ADVERTISEMENT_FILENAME) as f:
        return f.read()

def get_text_line(name):
    global text_lines
    global text_lines_file_updated_time
    if not os.path.getmtime(TEXT_LINES_FILENAME) == text_lines_file_updated_time:
        logging.info("loading ./fgis_volume/text_lines.json")
        text_lines_file_updated_time = os.path.getmtime(TEXT_LINES_FILENAME)
        with open(TEXT_LINES_FILENAME, 'r') as f:
            text_lines = json.loads(f.read())
    if name in text_lines:
        return text_lines[name]
    else:
        raise Exception("no such field in text_lines: {}".format(name))