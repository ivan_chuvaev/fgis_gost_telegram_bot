import json
import urllib.request
import threading
from src.volume_files import get_text_line
from src.config import *
import logging

class Local_chat_data:
	
	def __init__(self):
		self.stage_zero = {
			'year' : None,
			'mitnumber' : None   
		}
		self.stage_one = {
			'page_number' : 1,
			'page_count' : 1,
			'json_obj' : None,
			'paging_message_id' : None
		}
		self.stage = 0

	def reset_stage_one(self):
		self.stage_one = {
			'page_number' : 1,
			'page_count' : 1,
			'json_obj' : None,
			'paging_message_id' : None
		}

	#retrieving miOwner
	def set_miOwner(self, i):
		if not 'miOwner' in self.stage_one['json_obj']['response']['docs'][i]:
			try:
				elurl = get_text_line('device_info_json') % self.stage_one['json_obj']['response']['docs'][i]['vri_id']
				with urllib.request.urlopen( urllib.request.Request(elurl, headers=fgis_device_info_headers), timeout=fgis_request_timeout) as response:
					eljson = json.loads(response.read().decode('utf-8'))
					self.stage_one['json_obj']['response']['docs'][i]['miOwner'] = eljson['result']['vriInfo']['miOwner']    
			except Exception as e:
				logging.error(e)
				self.stage_one['json_obj']['response']['docs'][i]['miOwner'] = 'unknown'
		return

	def get_page(self, start_index, end_index):

		if start_index >= max_results_count:
			return 'Максимальное количество отображаемых результатов: ' + str(max_results_count)

		if end_index >= len(self.stage_one['json_obj']['response']['docs']):
			end_index = len(self.stage_one['json_obj']['response']['docs']) - 1

		docs_count = end_index - start_index + 1

		output_str = ""
		
		thread_arr = [None] * docs_count

		for i in range(docs_count):
			thread_arr[i] = threading.Thread(target=self.set_miOwner, args=[start_index + i])
			thread_arr[i].start()
		
		for i in range(docs_count):
			thread_arr[i].join()

		for i in range(start_index, end_index + 1):
			el = self.stage_one['json_obj']['response']['docs'][i]
			output_str += str(i + 1) + ' || ' + el['mi.number'] + ' || ' + el['mi.mititle'] + ' || ' + el['miOwner'] + '\n'

		return output_str