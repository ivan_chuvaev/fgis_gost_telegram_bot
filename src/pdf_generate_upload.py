from pdf_creation.fgis_generate_pdf import fgis_generate_pdf
from src.volume_files import get_text_line, get_message_advertisement
import logging
import os
from src.config import *
import urllib.request
import json
from telegram import ParseMode

def pdf_generate_upload(update, context, num):

    chat_id = update.effective_message.chat.id

    #generate pdf file and save it with name: 'tmp/' + vri_id + '.pdf'
    doc = context.chat_data['local_data'].stage_one['json_obj']['response']['docs'][num] #
    filename = 'tmp/' + doc['vri_id'] + '_' + str(chat_id) + '.pdf'
    if fgis_generate_pdf(doc['vri_id'], filename) == -1:
        update.message.reply_text(get_text_line('fgis_generate_pdf_error'))
        logging.error("failed to create pdf file in ./tmp folder")
        return -1


    #retrieving miOwner
    miOwner = 'unknown'
    try:
        doc_url = get_text_line('device_info_json') % doc['vri_id']
        with urllib.request.urlopen( urllib.request.Request(doc_url, headers=fgis_device_info_headers), timeout=fgis_request_timeout) as response:
            doc_json = json.loads(response.read().decode('utf-8'))
            miOwner = doc_json['result']['vriInfo']['miOwner']
    except Exception as e:
        logging.error(e)

    #send info about selected SI
    update.message.reply_text(
        get_text_line('response_for_selected') % (
            doc['mi.number'],
            doc['mi.mititle'],
            miOwner,
            doc['vri_id']
        ) + '\n\n' + get_message_advertisement(),
        parse_mode = ParseMode.MARKDOWN
    )

    #send pdf file to user

    try:

        file_message = context.bot.send_document(
            chat_id = update.message.chat.id,
            document = open(filename,'rb')
            )
        logging.info('sent file: {} to: @{}'.format(file_message['document']['file_name'], file_message['chat']['username']))

    except Exception as e:
        logging.error(e)

    #removing pdf file from local filesystem and reset stage
    os.remove(filename)