from telegram.ext import CallbackContext 
from telegram import InlineKeyboardMarkup, Update
from src.config import *
from src.volume_files import get_text_line
from src.fgis_search import fgis_search
import threading
import logging

#handler for interactive buttons
def button(update: Update, context: CallbackContext) -> None:

	query = update.callback_query
	
	# CallbackQueries need to be answered, even if no notification to the user is needed
	# Some clients may have trouble otherwise. See https://core.telegram.org/bots/api#callbackquery
	query.answer()
	reply_markup = InlineKeyboardMarkup(page_numbers)

	try:

		local_data = context.chat_data['local_data'].stage_one

		#if user pressed button which does not belong to active paging message then it should be removed
		if (update.callback_query['message']['message_id'] != local_data['paging_message_id']):
			raise

		#move between pages back and forth
		if(query.data == '1'):
			local_data['page_number']-=1
		else:
			local_data['page_number']+=1
		if(local_data['page_number'] > local_data['page_count']):
			local_data['page_number'] = local_data['page_count']
			return
		if(local_data['page_number'] < 1):
			local_data['page_number'] = 1
			return

		#update text of message, write list elements that belong to current page
		searching_page_message = get_text_line('page_message') % (
			local_data['page_number'],
			local_data['page_count'],
			local_data['json_obj']['response']['numFound']
			)
		start_index = (local_data['page_number'] - 1) * max_results_count_on_page
		searching_page_message += context.chat_data['local_data'].get_page(start_index, start_index + max_results_count_on_page - 1)
		query.edit_message_text(text=searching_page_message, reply_markup=reply_markup)

	except Exception as e:

		logging.info(e)
		#remove interactive buttons from message
		context.bot.edit_message_reply_markup(
			chat_id = update.callback_query['message']['chat']['id'],
			message_id = update.callback_query['message']['message_id'],
			reply_markup = None
		)


#response function
def echo(update, context):

	#prevent request while bot is processing previous message for specific user
	if not 'now_replying' in context.chat_data:
		context.chat_data['now_replying'] = []
	else:
		for el in context.chat_data['now_replying']:
			if el == update.effective_message.chat_id:
				update.message.reply_text(get_text_line("now_replying"))
				logging.info(
					"@{} requested {} but bot is still processing previous request"
					.format(update.message.from_user.username, update.message.text)
				)
				return
	logging.info("@{} requested {}".format(update.message.from_user.username, update.message.text))

	context.chat_data['now_replying'].append(update.effective_message.chat.id)

	#after recieving correct request bot will create new thread for searching
	thr1 = threading.Thread(target=fgis_search, args=[update, context])
	thr1.start()