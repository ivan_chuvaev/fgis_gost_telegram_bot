from src.volume_files import get_text_line

def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text=get_text_line('start_message'))

def reset_stage(update, context):
    try:
        if context.chat_data['local_data'].stage == 0:
            raise
        context.chat_data['local_data'].reset_stage_one()
        context.chat_data['local_data'].stage = 0
        context.bot.send_message(chat_id=update.effective_chat.id, text=get_text_line("stage reset"))
    except:
        context.bot.send_message(chat_id=update.effective_chat.id, text=get_text_line("nothing to reset"))