import urllib.request
import json
from math import ceil
import logging
from telegram import InlineKeyboardMarkup
from src.local_chat_data import Local_chat_data
from src.volume_files import get_text_line
from src.pdf_generate_upload import pdf_generate_upload
from src.config import *
from socket import timeout

#needs for http request search of fgis
def fl_generate(mitnumber, minumber):
	ret = 'fq=mi.mitnumber:' + mitnumber + '&'
	if minumber.find(' ') == -1:
		if minumber != '*':
			ret += 'fq=mi.number:' + minumber +'&'
		return ret
	for el in minumber.split(' '):
		ret += "fq=mi.number:*" + el + "*&"
	return ret

class MineError(Exception):
	pass

def fgis_search(update, context):

	#if there is no local data in chat then add it
	if not 'local_data' in context.chat_data:
		context.chat_data['local_data'] = Local_chat_data()
	local_data = context.chat_data['local_data'].stage_one


	############################################################################################
	#-------------------------------------stage 1-----------------------------------------------
	############################################################################################
	#accept number of element after searching is done
	if (context.chat_data['local_data'].stage == 1):
		try:
			num = int(update.message.text) - 1
			if (num < 0 or num >= len(local_data['json_obj']['response']['docs'])):
				raise Exception(get_text_line('number_out_of_range'))

			#remove interactive buttons from message
			try:

				context.bot.edit_message_reply_markup(
					chat_id = update.message.chat.id,
					message_id = local_data['paging_message_id'],
					reply_markup = None
				)
			except Exception as e:
				None

			#generating pdf and sending it to user
			pdf_generate_upload(update, context, num)
			context.chat_data['local_data'].reset_stage_one()
			context.chat_data['local_data'].stage = 0  

		except ValueError:
			update.message.reply_text(get_text_line('incorrect_number'))
		except Exception as e:
			logging.error("error while processing fgis search on stage 1, {}".format(e))
			update.message.reply_text(str(e))

		context.chat_data['now_replying'].remove(update.effective_message.chat.id)
		return


	############################################################################################
	#-------------------------------------stage 0-----------------------------------------------
	############################################################################################
	#accept year, fif and factory number: (year, mi.mitnumber, mi.number)
	if (context.chat_data['local_data'].stage == 0):
		try:

			si = update.message.text.find(' ')

			#only minumber
			if si == -1:
				if context.chat_data['local_data'].stage_zero['year'] == None:
					raise Exception(get_text_line('year_missing'))
				arr = [context.chat_data['local_data'].stage_zero['year'], context.chat_data['local_data'].stage_zero['mitnumber'], update.message.text.strip()]

			#full search: year, mitnumber, minumber
			else:
				arr = update.message.text.split(' ')
				if len(arr) > 3:
					raise Exception(get_text_line('space_count>3'))
				if len(arr) < 3:
					raise Exception(get_text_line('space_count<3'))

				try:
					int(arr[0])
				except:
					raise Exception(get_text_line("incorrect input"))

				context.chat_data['local_data'].stage_zero['year'] = arr[0]
				context.chat_data['local_data'].stage_zero['mitnumber'] = arr[1]



			#generate url to get results of search, request it and parse response
			url = get_text_line('http_search') % (arr[0], fl_generate(arr[1], arr[2]), max_results_count)

			logging.info('searching: {}'.format(url))

			with urllib.request.urlopen(urllib.request.Request(url, headers=fgis_search_headers), timeout=fgis_request_timeout) as response:
				res_str = response.read().decode('utf-8')
				local_data['json_obj'] = json.loads(res_str)

				if (local_data['json_obj']['response']['numFound'] == 0):
					raise Exception(get_text_line("no results found"))

				#if there is only one result then generate pdf file right now and don't go to stage one
				if (local_data['json_obj']['response']['numFound'] == 1):
					pdf_generate_upload(update, context, 0)
					raise MineError() #jump out

				local_data['page_count'] = ceil(local_data['json_obj']['response']['numFound']/max_results_count_on_page)
				logging.info("found {} results".format(local_data['json_obj']['response']['numFound']))

				output_str = get_text_line('page_message') % (
					local_data['page_number'],
					local_data['page_count'],
					local_data['json_obj']['response']['numFound']
					)

				#get string with all elements on current page
				output_str += context.chat_data['local_data'].get_page(0, max_results_count_on_page - 1)

				#add interactive buttons: forward, backward. only when page_count > 1

				reply_markup = (None, InlineKeyboardMarkup(page_numbers))[local_data['page_count'] > 1]

				context.chat_data['local_data'].stage_one['paging_message_id'] = \
					(update.message.reply_text(output_str, reply_markup=reply_markup)).message_id

				context.chat_data['local_data'].stage = 1


		except MineError: #goto
			pass
		except ValueError:
			logging.error("incorrect input on stage 0")
			context.bot.send_message(chat_id=update.effective_chat.id, text=get_text_line('stage_one_incorrect_input'))
		except timeout as e:
			logging.error("{}".format(e))
			context.bot.send_message(chat_id=update.effective_chat.id, text=get_text_line('timeout'))
		except (urllib.error.HTTPError, urllib.error.URLError) as e:
			logging.error("{}".format(e))
			context.bot.send_message(chat_id=update.effective_chat.id, text=get_text_line('server_cant_process_request'))
		except Exception as e:
			logging.error("incorrect input on stage 0: {}".format(e))
			context.bot.send_message(chat_id=update.effective_chat.id, text=str(e))

	context.chat_data['now_replying'].remove(update.effective_message.chat.id)
	return
