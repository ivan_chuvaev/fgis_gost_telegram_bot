from telegram import InlineKeyboardButton

max_results_count_on_page = 10
max_results_count = 100
fgis_request_timeout = 60 #seconds
pub_fsa_gov_ru_request_timeout = 10 #seconds
max_bytes_for_log_file = 10000
max_log_file_backups = 10

fgis_search_headers = {
    'authority': 'fgis.gost.ru',
    'method': 'GET',
    'scheme': 'https',
    'accept': 'application/json, text/plain, */*',
    'accept-encoding': 'UTF-8',
    'accept-language': 'en-US,en;q=0.9,ru;q=0.8',
    'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
    'sec-ch-ua-mobile': '?0',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
}

fgis_device_info_headers = {
    'authority': 'fgis.gost.ru',
    'method': 'GET',
    'scheme': 'https',
    'accept': 'application/json, text/plain, */*',
    'accept-encoding': 'UTF-8',
    'accept-language': 'en-US,en;q=0.9,ru;q=0.8',
    'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
    'sec-ch-ua-mobile': '?0',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
}

#buttons
page_numbers = [
    [
        InlineKeyboardButton("<", callback_data='1'),
        InlineKeyboardButton(">", callback_data='2'),
    ]
]