from telegram.ext import Updater, MessageHandler, Filters, CommandHandler, CallbackQueryHandler
from flask import Flask
import threading

import logging
from logging.handlers import RotatingFileHandler

from src.command_handers import *
from src.config import *
from src.handlers import *

import ssl
ssl._create_default_https_context = ssl._create_unverified_context

if __name__=="__main__":

	filehandler = RotatingFileHandler("./fgis_volume/logs/logfile.log", mode='a', maxBytes=max_bytes_for_log_file, backupCount=max_log_file_backups)
	logging.basicConfig(
		handlers=[filehandler],
		format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
		level=logging.INFO
	)

	with open('./fgis_volume/token', 'r') as f:
		bot_token = f.read().strip()

	app = Flask(__name__)
	@app.route("/")
	def hello_world():
		return get_text_line('fgis_gost_bot_running')
	

	updater = Updater(token=bot_token, use_context=True)
	dispatcher = updater.dispatcher
	dispatcher.add_handler(MessageHandler(Filters.text & (~Filters.command), echo))
	dispatcher.add_handler(CallbackQueryHandler(button, pass_chat_data=True, pass_update_queue=True))
	dispatcher.add_handler(CommandHandler('start', start))
	dispatcher.add_handler(CommandHandler('resetstage', reset_stage))


	#all the work will be happening inside main_second_thread thread
	main_second_thread = threading.Thread(target=updater.start_polling, kwargs={'poll_interval' : 1.0})
	main_second_thread.start()
